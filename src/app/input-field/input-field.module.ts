import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputFieldComponent } from './input-field/input-field.component';
import { InputFieldRoutingModule } from './input-field-routing.module';



@NgModule({
  declarations: [InputFieldComponent],
  imports: [
    CommonModule,
    InputFieldRoutingModule
  ]
})
export class InputFieldModule { }
