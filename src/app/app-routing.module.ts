import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InputFieldModule } from './input-field/input-field.module';


const routes: Routes = [
  {path: '', loadChildren: () => import('./input-field/input-field.module').then(
    m => m.InputFieldModule
  ) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
